When compiling and executing this python program, few things need to be noted: 


For OBST_serial.py: this program is implemented in serial. Run command: 
	python OBST_serial.py


For OBST_serial_testcase.py: this program is implemented in serial and set a fixed dataset to test. Run command: 
	python OBST_serial_testcase.py


For OBST.py:
1). Install mpi,mpi4py. Users can install in Anaconda with below sentence:
    pip install mpi mpi4py dumpy time
2). Run the program in terminal with command line: 
    mpirun -n 4 python OBST.py 10
Note: 
--mpirun: or mpiexec 
--4: the number of processors, users can change to 8
--10: the size of dataset

3). The output displays 3 kinds of datasets on the terminal:
--original dataset
--probability of each data: data_prob
--Initial probability table: prob_table
--Initial root index table: root_table
--Final probability table: prob_table
--Final root index table: root_table
--The level of each data: level