"""
This program implements OBST in serial. 
@author: SophieHou (lh6032)
"""

import numpy as np, numpy.random
import sys
import time
 
def makeData(size, data):
    """
    Generate raw datasets randomly. 
    """
    np.random.seed(10) 
    i = 0
    while i < size and len(data) < size:
        item = np.random.randint(0, size*5)
        if item not in data:
            data.append(item)
            i += 1
        else:
            i -= 1
    data.sort()
    print("Original Dataset:", data)
    return data

def makeTables(size):
    rows = size+2
    cols = size+1
    prob_table = [[0 for i in range(cols)] for j in range(rows)] 
    root_table = [[0 for i in range(cols)] for j in range(rows)] 
    np.random.seed(3) 
    data_prob = np.round(np.random.dirichlet(np.ones(size), size=1), 3).flatten()

    for i in range(1, cols):
        prob_table[i][i] = data_prob[i-1]
        root_table[i][i] = i
    return prob_table, root_table, data_prob


def getMinCost(sIndex, eIndex, prob_table, root_table, data_prob):
    costs = []

    for i in range(sIndex, eIndex+1):
        temp_cost = prob_table[sIndex][i-1] + prob_table[i+1][eIndex] + data_prob[sIndex-1:eIndex].sum()
        costs.append(temp_cost)
    for i, v in enumerate(costs):
        if v == min(costs):
            prob_table[sIndex][eIndex] = round(v,5)
            root_table[sIndex][eIndex] = sIndex + i   


def getLevelOfEachData(root_table, data_prob, prob_table, data, size):
    root_index = root_table[1][size]
    level = {}
    level[root_index] = 1
    # left tree of root
    for j in range(size, 0, -1):
        temp = root_table[1][j]
        if temp != root_index:
            level[temp] = level.get(root_index)+1
            root_index = temp    
    root_index = root_table[1][size]
    # right tree of root
    for i in range(2, size+1):
        temp = root_table[i][size]
        if temp != root_index:
            level[temp] = level.get(root_index)+1
            root_index = temp
    # subtree of left and right
    for i in range(2, size+1):
        for j in range(size, i-1, -1):
            temp = root_table[i][j]
            if temp not in level:
                if root_table[i-1][j+1] == root_table[i][j+1]:
                    level[temp] = level.get(root_table[i-1][j])+1
                else:
                    level[temp] = level.get(root_table[i][j+1])+1
    print("\nlevel=", level)

    ttl_cost = 0

    for i in level:
        print("The", i, "th data", data[i-1], "is at level", level[i])


def main(argv):
    if len(argv) < 2:
        print("Command Error. Please Input Number of Keys.")

    size = int(argv[1]) 
    data = makeData(size, []) # create sorted dataset
    prob_table, root_table, data_prob = makeTables(size)
    count = 0
    while count < size-1:
        for i in range(1, size):
            j = i+count+1
            if j <= size:
                getMinCost(i, j, prob_table, root_table, data_prob)
        count += 1
    print("\nFinal prob_table= ", prob_table)        
    print("\nFinal root_index_table= ", root_table) 
    print("\nThe final total cost is at the right cornor, which equals to prob_table[1][size]=", prob_table[1][size])
    getLevelOfEachData(root_table, data_prob, prob_table, data, size)


if __name__ == '__main__':
    start = time.time()     # start time
    main(sys.argv)
    print("runtime",time.time()-start)

