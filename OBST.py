"""
This program implements OBST in parallel by calling MPI. 
@author: SophieHou (lh6032)
"""
# pip install mpi mpi4py
# command: mpirun -n 4 python OBST.py 16 # on a laptop
# or mpiexec


from mpi4py import MPI
# from scipy.stats import norm
import numpy as np, numpy.random
import sys
import time

comm = MPI.COMM_WORLD
numproces = comm.Get_size() # num of processors
rank = comm.Get_rank() # processor name
itemsize = MPI.DOUBLE.Get_size()

def makeData(size, data):
    """
    Generate raw datasets randomly.
    """
    data = []
    np.random.seed(10) 
    i = 0
    while i < size and len(data) < size:
        item = np.random.randint(0, size*5)
        if item not in data:
            data.append(item)
            i += 1
        else:
            i -= 1
    data.sort()
    print("Original Dataset:", data)
    return data

def makeTables(size):
    rows = size+2
    cols = size+1
    prob_table = [[0 for i in range(cols)] for j in range(rows)] 
    root_table = [[0 for i in range(cols)] for j in range(rows)] 
    np.random.seed(3) 
    data_prob = np.round(np.random.dirichlet(np.ones(size),size=1),3).flatten()

    for i in range(1, cols):
        prob_table[i][i] = data_prob[i-1]
        root_table[i][i] = i
    return prob_table, root_table, data_prob


def getMinCost(sIndex, eIndex, prob_table, root_table, data_prob):
    costs = []

    for i in range(sIndex, eIndex+1):
        temp_cost = prob_table[sIndex][i-1] + prob_table[i+1][eIndex] + data_prob[sIndex-1:eIndex].sum()
        costs.append(temp_cost)
    for i, v in enumerate(costs):
        if v == min(costs):
            prob_table[sIndex][eIndex] = round(v,5)
            root_table[sIndex][eIndex] = sIndex + i


def getLevelOfEachData(root_table, data_prob, prob_table, data, size):
    root_index = root_table[1][size]
    level = {}
    level[root_index] = 1
    # left tree of root
    for j in range(size, 0, -1):
        temp = root_table[1][j]
        if temp != root_index:
            level[temp] = level.get(root_index)+1
            root_index = temp    
    root_index = root_table[1][size]
    # right tree of root
    for i in range(2, size+1):
        temp = root_table[i][size]
        if temp != root_index:
            level[temp] = level.get(root_index)+1
            root_index = temp
    # subtree of left and right
    for i in range(2, size+1):
        for j in range(size, i-1, -1):
            temp = root_table[i][j]
            if temp not in level:
                if root_table[i-1][j+1] == root_table[i][j+1]:
                    level[temp] = level.get(root_table[i-1][j])+1
                else:
                    level[temp] = level.get(root_table[i][j+1])+1
    print("\nlevel=", level)

    ttl_cost = 0

    for i in level:
        print("The", i, "th data", data[i-1], "is at level", level[i])


def main(argv, runtime):
    start = time.time()     # start time

    if len(argv) < 2:
        print("Command Error. Please Input Number of Keys.")
    size = int(argv[1])
    per1 = size // numproces + size % numproces  # the range of index for each process to handle
    per2 = per1 // 2 + per1 % 2  # half of the range
    if rank == 0:
        shared = (size+2)*(size+1) * itemsize  # set shared size  
    else:
        shared = 0
    
    window = MPI.Win.Allocate_shared(shared, itemsize, comm=comm)
    window1 = MPI.Win.Allocate_shared(shared, itemsize, comm=comm)
    window2 = MPI.Win.Allocate_shared(shared, itemsize, comm=comm)

    buff, numprocess = window.Shared_query(0)
    prob_table = np.ndarray(buffer=buff, dtype="d", shape=(size+2, size+1))
    buff1, numprocess = window1.Shared_query(0)
    root_table = np.ndarray(buffer=buff1, dtype="i", shape=(size+2, size+1))
    buff2, numprocess = window2.Shared_query(0)
    data_prob = np.ndarray(buffer=buff2, dtype="d", shape=(size,))
    
 
    if rank == 0:
        data = makeData(size, []) # create sorted dataset
        prob_table[:size+2][:size+2], root_table[:size+2][:size+2], data_prob[:size] = makeTables(size)
        print("data_prob=",data_prob)
        print("\nInitial prob_table=",prob_table)
        print("\nInitial root_table=",root_table)

    comm.Barrier()
    count1 = 0
    while count1 < size-1:
        for i in range(per2*rank+1, per2*(rank+1)+1):
            j = i+count1+1
            if j <= size:
                print("S-(i,j)=",i,j, ",rank=", rank)
                getMinCost(i, j, prob_table, root_table, data_prob)
        for i in range(size-per2*(rank+1), size-per2*rank):
            j = i+count1+1
            if j <= size:
                print("E-(i,j)=",i,j, ",rank=", rank)
                getMinCost(i, j, prob_table, root_table, data_prob)
        comm.Barrier()  
        count1 += 1
        comm.Barrier()
  
    print("\nFinal prob_table=",prob_table, ", rank name = ", rank)
    print("\nFinal root_index_table=",root_table, ", rank name = ", rank)
    
    if rank == 0:
        print("\nThe final total cost is at the right cornor, which equals to prob_table[1][size]=", prob_table[1][size])
        getLevelOfEachData(root_table, data_prob, prob_table, data, size)
        runtime.append(np.round(time.time()-start,3))


if __name__ == '__main__':
    runtime=[]
    start = time.time()
    # for i in range(19): # check 19 runs
    main(sys.argv, runtime)
    print("runtime", time.time()-start)

    # if rank==0:
    #     print("\n\nMean of 19 runs:", np.mean(runtime))

